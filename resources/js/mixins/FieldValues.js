export default {
    data() {
        return {
            options: []
        }
    },

    methods: {
        setValueFromField() {
            this.value = ''
            if (this.field.value) {
                this.value = JSON.parse(this.field.value)
            }
        },
        async setInitialOptions(contentType) {
            if (this.value.length > 0 && this.route) {
                Nova.request().get(this.route, {
                    params: {
                        'value': this.field.value
                    }
                }).then(response => {
                    this.options = response.data
                    this.value = response.data

                    this.initialOptionsSet = true;
                })
            }
        }
    },

    computed: {
        idColumn() {
            return this.field.modelIdColumn || 'id'
        },
        label() {
            return this.field.searchLabel || 'name'
        },
        route() {
            if (this.field.contentType) {
                return this.field.route + this.field.contentType
            }

            if (this.dependentFieldValue) {
                return this.field.route + this.dependentFieldValue
            }

            return this.field.route
        }
    }
}
