Nova.booting((Vue, router, store) => {
    Vue.component('index-multiselect', require('./components/IndexField').default)
    Vue.component('detail-multiselect', require('./components/DetailField').default)
    Vue.component('form-multiselect', require('./components/FormField').default)
  })