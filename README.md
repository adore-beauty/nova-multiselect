# Laravel Nova Multi-select Field

This Nova custom field is used to create a Multi Select that loads its options via the API.
We can either specify the content type that should be loaded, or determine it by referencing a dependent field.


# Examples

### Example using static `contentType` method to specify the API route.

```php
use Adorebeauty\Multiselect\Multiselect;

Multiselect::make('{Field Name}', '{field_name}')
    ->optionsLabel('name')                      // options key name
    ->optionsIdField('id')                      // options value field
    ->modelIdColumn('id')                       // id of the vue-multiselect model
    ->route('{route}')                          // route to which vue should send request to e.g. /api/nova-multiselect
    ->placeholder('{Place Holder}')             // placeholder
    ->contentType('brands')                     // the value of this field will be send to the route as content type
    ->customLabel('${label} - ${id}')           // optional custom format for label text, with ${label} and ${id} tags
    ->maxOptions({$limit})                      // limit per request
```


### Example using the `dependsOn` method to register a dependent field.

```php
use Adorebeauty\Multiselect\Multiselect;

Multiselect::make('{Field Name}', '{field_name}')
    ->optionsLabel('name')
    ->optionsIdField('id')
    ->modelIdColumn('id')
    ->route('{route}')
    ->placeholder('{Place Holder}')
    ->dependsOn('item_matcher_type', () => {})  // the value of this field will be appended to the API route
    ->initialContentType(
        self::find($request->resourceId)?->item_matcher_type ?? null
    )                                           // used for the detail view
    ->customLabel('${label} - ${id}')
    ->maxOptions({$limit})
```


# Developer notes

- We used the Vue Options API because we are pulling in Nova mixins in the Form view - https://nova.laravel.com/docs/4.0/customization/frontend.html#using-nova-mixins
- The **vue-multiselect** npm package is used to provide the drop-down functionality - https://www.npmjs.com/package/vue-multiselect

### Note about dependent fields

- For the Form view we register a Nova event listener to listen for changes in the dependent field - https://nova.laravel.com/docs/4.0/customization/frontend.html#event-bus

- In the detail view the dependent field does not emit events.
  - Use the `initialContentType()` option to tell Vue which API route it should use to hydrate the selected options.
  - The `initialContentType` needs to be loaded during page load, e.g. make an Eloquent query inside the Nova Resource.

### Local development

We can preview this package locally in a Laravel project - this setup works with Laravel Sail.

- Clone this package repository into the `/nova-components/nova-multiselect` folder in your Laravel project
- Add the `/nova-components` folder to .gitignore
- Add this JSON snippet to the top of the `repositories` array in the `composer.json` file of the parent project

```json
{
    "type": "path",
    "url": "./nova-components/nova-multiselect",
    "options": {
        "symlink": true
    }
}
```

- In the parent project, update composer packages

```shell
composer clearcache
composer update
```

- Build the component

```shell
cd nova-components/nova-multiselect
npm run dev
```
