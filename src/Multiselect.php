<?php
namespace Adorebeauty\Multiselect;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\SupportsDependentFields;

class Multiselect extends Field
{
    use SupportsDependentFields;

    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'multiselect';

    /**
     * Set search contentType
     *
     * @param string $contentType
     * @return $this
     */
    public function optionsContentType(string $contentType): Multiselect
    {
        return $this->withMeta(['contentType' => $contentType]);
    }

    /**
     * Set label for searching and frontend output
     *
     * @param string $searchLabel
     * @return $this
     */
    public function optionsLabel(string $searchLabel): Multiselect
    {
        return $this->withMeta(['searchLabel' => $searchLabel]);
    }

    /**
     * Set id for searching and frontend output
     *
     * @param string $idField
     * @return $this
     */
    public function optionsIdField(string $idField): Multiselect
    {
        return $this->withMeta(['idField' => $idField]);
    }

    /**
     * Set id for the vue multiselect form
     *
     * @param string $column
     * @return $this
     */

    public function modelIdColumn(string $column): Multiselect
    {
        return $this->withMeta(['modelIdColumn' => $column]);
    }

    /**
     * Set max Option while search which will be passed to the route
     *
     * @param integer $max
     * @return Multiselect
     */
    public function maxOptions(int $max): Multiselect
    {
        return $this->withMeta(['max' => $max]);
    }

    /**
     * placeholder for the multiselect
     *
     * @param string $placeholder
     * @return Multiselect
     */
    public function placeholder($placeholder): Multiselect
    {
        return $this->withMeta(['placeholder' => $placeholder]);
    }

    /**
     * router to which the multiselect will query for options
     *
     * @param string $router
     * @return Multiselect
     */

    public function route(string $router = ''): Multiselect
    {
        return $this->withMeta(['route' => $router]);
    }

    /**
     *  @param boolean $field
     * @return Multiselect
     */
    public function searchable(bool $field): Multiselect
    {
        return $this->withMeta(['searchable' => $field]);
    }

    /**
     * Use a custom label for the multiselect element using ${id} and ${label} tags
     * E.g. '${label} - ${id}'
     *
     * @param string $format
     * @return Multiselect
     */
    public function customLabel(string $format): Multiselect
    {
        return $this->withMeta(['customLabel' => $format]);
    }

    /**
     * @param string|null $contentType
     * @return Multiselect
     */
    public function initialContentType(?string $contentType): Multiselect
    {
        return $this->withMeta(['initialContentType' => $contentType]);
    }
}
